import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by greg on 19.01.16.
 */
public class TetrisGame extends JPanel {
    public static final int WIDTH = 700;
    private static final int INPUTHEIGHT = 24;
    private static final int BORDER = 100;

    private JTextField aggregateHeight;
    private JTextField fullLines;
    private JTextField bumpiness;
    private JTextField holes;
    private JButton startBest;
    private JButton start;

    private JPanel controlPanel;

    public TetrisGame() {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        setPreferredSize(new Dimension(WIDTH, screenSize.height - BORDER));
        setLayout(new BorderLayout());

        aggregateHeight = new JTextField("AggregateHeightWage", 50);
        aggregateHeight.setPreferredSize(new Dimension(WIDTH / 4, INPUTHEIGHT));

        fullLines = new JTextField("FullLinesWage", 50);
        fullLines.setPreferredSize(new Dimension(WIDTH / 4, INPUTHEIGHT));

        bumpiness = new JTextField("BumpinessWage", 50);
        bumpiness.setPreferredSize(new Dimension(WIDTH / 4, INPUTHEIGHT));

        holes = new JTextField("HolesWage", 50);
        holes.setPreferredSize(new Dimension(WIDTH / 4, INPUTHEIGHT));


        startBest = new JButton("Start best");
        start = new JButton("Start");

        startBest.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Parameters params = new Parameters(-7.177496533552712E8, 7.790532261831613E8, -1.7531504216164482E9, -5.660727911995431E8);
                Game game = new Game(params);
                Thread thread = new Thread(game);
                thread.start();
                add(game);
                revalidate();
            }
        });

        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Parameters params = new Parameters(Double.parseDouble(aggregateHeight.getText()), Double.parseDouble(fullLines.getText()),
                        Double.parseDouble(holes.getText()), Double.parseDouble(bumpiness.getText()));
                Game game = new Game(params);
                Thread thread = new Thread(game);
                thread.start();
                add(game);
                revalidate();
            }
        });

        controlPanel = new JPanel();
        controlPanel.setLayout(new GridLayout(2, 5));
        controlPanel.setPreferredSize(new Dimension(WIDTH, 50));

        controlPanel.add(aggregateHeight);
        controlPanel.add(fullLines);
        controlPanel.add(holes);
        controlPanel.add(bumpiness);
        controlPanel.add(startBest);
        controlPanel.add(start);

        add(controlPanel, BorderLayout.NORTH);
    }
}
