import java.util.Random;

/**
 * Created by greg on 25.12.15.
 */
public class Individual implements Runnable {
    private Parameters params;
    private int fitnessMeter;
    private Random randMachine;
    private int gamesCounter;

    public Individual() {
        this.randMachine = new Random();
        this.params = new Parameters(1, 1, 1, 1);
        this.gamesCounter = 20;
        this.generateParams();
    }

    public Individual(int gamesCounter) {
        this.randMachine = new Random();
        this.params = new Parameters(1, 1, 1, 1);
        this.gamesCounter = gamesCounter;
        this.generateParams();
    }

    public Individual(Individual ind) {
        this.params = ind.params;
        this.fitnessMeter = ind.fitnessMeter;
        this.randMachine = ind.randMachine;
        this.gamesCounter = ind.gamesCounter;
    }

    public Parameters getGenome() {
        return this.params;
    }

    public void setGenome(Parameters params) {
        this.params = params;
    }

    public int getFitness() {
        return this.fitnessMeter;
    }

    public void generateParams() {
        int aggregateHeight = randMachine.nextInt();
        int fullLines = randMachine.nextInt();
        int holes = randMachine.nextInt();
        int bumpiness = randMachine.nextInt();

        boolean negative = randMachine.nextBoolean();
        if(negative) params.setAggregateHeightWage(-1*aggregateHeight);
        else params.setAggregateHeightWage(aggregateHeight);

        negative = randMachine.nextBoolean();
        if(negative) params.setBumpinessWage(-1*bumpiness);
        else params.setBumpinessWage(bumpiness);

        negative = randMachine.nextBoolean();
        if(negative) params.setHolesWage(-1*holes);
        else params.setHolesWage(holes);

        negative = randMachine.nextBoolean();
        if(negative) params.setFullLinesWage(-1*fullLines);
        else params.setFullLinesWage(fullLines);
    }

    public void mutate() {
        int parameter = randMachine.nextInt(4);
        boolean negative = randMachine.nextBoolean();
        switch (parameter) {
            case 0:
                double aggregateHeight = params.getAggregateHeightWage();
                if(negative) aggregateHeight -= 0.2;
                else aggregateHeight += 0.2;
                params.setAggregateHeightWage(aggregateHeight);
                break;
            case 1:
                double fullLines = params.getFullLinesWage();
                if(negative) fullLines -= 0.2;
                else fullLines += 0.2;
                params.setFullLinesWage(fullLines);
                break;
            case 2:
                double holes = params.getHolesWage();
                if(negative) holes -= 0.2;
                else holes += 0.2;
                params.setHolesWage(holes);
                break;
            case 3:
                double bumpiness = params.getBumpinessWage();
                if(negative) bumpiness -= 0.2;
                else bumpiness += 0.2;
                params.setBumpinessWage(bumpiness);
                break;
        }
    }

    public void run() { // runs n-games to calculate fitness of individual
            this.fitnessMeter = 0;
            for(int i = 0; i < this.gamesCounter; i++) {
                Game game = new Game(this.params);
                this.fitnessMeter += game.makeGame();
            }
    }
}
