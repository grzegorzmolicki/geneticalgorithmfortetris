import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Created by greg on 04.01.16.
 */
public class Game extends JPanel implements Runnable {
    private static final int STARTX = 250;
    private static final int STARTY = 100;
    private static final int BLOCKSIDE = 20;

    private int linesCounter;
    private int blocksCounter;

    CurrentBlock block;
    AI ai;
    int[][] grid = new int[21][10];

    private boolean didLost() {
        for (int i = 0; i < 10; i++) {
            if (this.grid[0][i] != 0) return true;
        }
        return false;
    }

    public Game(Parameters params) {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();
        setPreferredSize(new Dimension(WIDTH, screenSize.height - 100));
        this.linesCounter = 0;
        this.blocksCounter = 0;
        this.block = new CurrentBlock(1);
        this.ai = new AI(params);
        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 10; j++)
                this.grid[i][j] = 0;
        for (int i = 0; i < 10; i++)
            grid[20][i] = 1;
    }

    public int makeGame() {
        int lines = 0;
        int bricksCounter = 0;
        while (!didLost() && bricksCounter < 10000) {
            bricksCounter++;
            ai.makeAIMove(this.grid, this.block);
            lines += ai.checkLine(this.grid);
        }
        return lines;
    }

    public void run() {
        try {
            this.blocksCounter = 0;
            this.linesCounter = 0;
            while (!didLost()) {
                this.blocksCounter++;
                repaint();
                Thread.sleep(100);
                ai.makeAIMove(this.grid, this.block);
                repaint();
                Thread.sleep(100);
                this.linesCounter += ai.checkLine(this.grid);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.draw(new Rectangle2D.Double(STARTX, STARTY, 10 * BLOCKSIDE, 20 * BLOCKSIDE));
        g.clearRect(0, 90, 200, 200);
        g2.drawString("Lines: " + this.linesCounter, 10, 100);
        g2.drawString("Bricks: " + this.blocksCounter, 10, 140);
        for (int i = 0; i < 20; i++)
            for (int j = 0; j < 10; j++)
                if (grid[i][j] != 0) {
                    g2.setPaint(Color.BLACK);
                    g2.fill(new Rectangle2D.Double(STARTX + j * BLOCKSIDE, STARTY + i * BLOCKSIDE, BLOCKSIDE, BLOCKSIDE));
                    g2.setPaint(Color.GRAY);
                    g2.fill(new Rectangle2D.Double(STARTX + j * BLOCKSIDE + 1, STARTY + i * BLOCKSIDE + 1, BLOCKSIDE - 2, BLOCKSIDE - 2));
                } else {
                    g2.setPaint(Color.BLACK);
                    g2.fill(new Rectangle2D.Double(STARTX + j * BLOCKSIDE, STARTY + i * BLOCKSIDE, BLOCKSIDE, BLOCKSIDE));
                }
    }
}

