import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by greg on 19.01.16.
 */
public class ActionFrame extends JFrame {
    TetrisGame tetrisGame = new TetrisGame();
    AboutSimulation aboutSimulation = new AboutSimulation();
    AboutTetris aboutTetris = new AboutTetris();
    Simulation simulation = new Simulation();

    public ActionFrame() {
        super("Genetic Algorithm developer for tetris game");
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        JMenu simulation = new JMenu("Simulation");
        JMenu game = new JMenu("Game");

        menuBar.add(simulation);
        menuBar.add(game);

        JMenuItem aboutSimulation = new JMenuItem("About Simulation");
        JMenuItem simulationWindow = new JMenuItem("Simulation Window");

        simulation.add(simulationWindow);
        simulation.add(aboutSimulation);

        JMenuItem aboutGame = new JMenuItem("About Tetris");
        JMenuItem startGame = new JMenuItem("Tetris");

        game.add(startGame);
        game.add(aboutGame);

        tetrisGame = new TetrisGame();
        add(tetrisGame);

        simulationWindow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            loadSimulationProcessWindow();
            }
        });

        aboutSimulation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadAboutSimulationWindow();
            }
        });

        startGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadTetrisGameWindow();
            }
        });

        aboutGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                loadAboutTetrisWindow();
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);

    }

    public void loadAboutTetrisWindow() {
        remove(tetrisGame);
        remove(aboutSimulation);
        remove(aboutTetris);
        remove(simulation);
        if(aboutTetris == null) aboutTetris = new AboutTetris();
        add(aboutTetris);
        revalidate();
        repaint();
    }

    public void loadTetrisGameWindow() {
        remove(tetrisGame);
        remove(aboutSimulation);
        remove(aboutTetris);
        remove(simulation);
        if(tetrisGame == null) tetrisGame = new TetrisGame();
        add(tetrisGame);
        revalidate();
        repaint();
    }

    public void loadAboutSimulationWindow() {
        remove(tetrisGame);
        remove(aboutSimulation);
        remove(aboutTetris);
        remove(simulation);
        if(aboutSimulation == null) aboutSimulation = new AboutSimulation();
        add(aboutSimulation);
        revalidate();
        repaint();
    }

    public void loadSimulationProcessWindow() {
        remove(tetrisGame);
        remove(aboutSimulation);
        remove(aboutTetris);
        remove(simulation);
        if(simulation == null) simulation = new Simulation();
        add(simulation);
        revalidate();
        repaint();
    }
}
