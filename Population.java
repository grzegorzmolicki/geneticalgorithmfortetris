import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by greg on 25.12.15.
 */
public class Population implements Runnable {
    private LinkedList<Individual> individuals = new LinkedList<>();
    private Random randMachine;
    private int populationSize;
    private int maxThreads;
    private JLabel generationsCounterLabel;
    private JProgressBar generationProgressBar;
    private int generationCounter;
    private int gamesLimit;
    private JTextArea fittestStats;

    public Population(int populationSize, int gamesLimit, int maxThreads, boolean initialise, JLabel generationsCounterLabel, JProgressBar generationProgressBar, JTextArea fittestStats) { // second constructor for swing/awt app, with jlabel to update
        this.gamesLimit = gamesLimit;
        this.fittestStats = fittestStats;
        this.generationProgressBar = generationProgressBar;
        this.generationsCounterLabel = generationsCounterLabel;
        this.maxThreads = maxThreads;
        this.populationSize = populationSize;
        this.generationCounter = 0;
        if(initialise)
            for(int i = 0; i < populationSize; i++) {
                Individual in = new Individual(this.gamesLimit);
                individuals.add(in);
            }
        this.randMachine = new Random();
    }

    public Individual getFittest() {
        Individual fittest = individuals.get(0);
        for(Individual ind : individuals) {
            if(ind.getFitness() > fittest.getFitness()) fittest = ind;
        }
        return fittest;
    }

    public void run() {
        Individual best = new Individual(getFittest());
        while(!Thread.currentThread().isInterrupted()) {
            generationsCounterLabel.setText("Generation: " + generationCounter);
            letThemFight();
            crossPopulation();
            generationCounter++;
            if(best.getFitness() < getFittest().getFitness()) best = new Individual(getFittest());
            fittestStats.setText("Best so far: " + best.getFitness() +
                    "\nAggregateHeight: " + best.getGenome().getAggregateHeightWage() +
                    "\nBumpiness: " + best.getGenome().getBumpinessWage() +
                    "\nHoles: " + best.getGenome().getHolesWage() +
                    "\nFullLines: " + best.getGenome().getFullLinesWage() + "\n");
        }
    }

    public void letThemFight() {
        int finishedThreads = 0;
        int availableThreads = 0;
        LinkedList<Thread> threads = new LinkedList<>();
        for(Individual ind : this.individuals) threads.add(new Thread(ind));

        boolean allTerminated = false;

        // I believe only one assigning to variable is faster than calling size() method every single time
        int threadSize = threads.size();
        while(!allTerminated) {
            allTerminated = true;
            finishedThreads = 0;
            for(Thread t : threads)
                if(!t.getState().equals(Thread.State.TERMINATED))
                    allTerminated = false;
                else
                    finishedThreads++;

            int finishedPercentage = finishedThreads * 100 / threadSize;

            this.generationProgressBar.setValue(finishedPercentage);
            this.generationProgressBar.setString("Finished: " + finishedPercentage + "% of generation.");

            if(!allTerminated) {
                int runningThreads = 0;
                for(int i = 0; i < threadSize; i++) {
                    if(!threads.get(i).getState().equals(Thread.State.NEW) &&
                            !threads.get(i).getState().equals(Thread.State.TERMINATED))
                        runningThreads++;
                }
                availableThreads = this.maxThreads - runningThreads;
                runningThreads = 0;
                for(int i = 0; (i < threadSize) && (runningThreads < availableThreads); i++) {
                    if(threads.get(i).getState().equals(Thread.State.NEW)) {
                        threads.get(i).start();
                        runningThreads++;
                    }
                }
            }
        }
    }

    public void crossPopulation() {
        LinkedList<Individual> offsprings = new LinkedList();
        while(offsprings.size() <= populationSize * 0.3) {
            int fittest = 0;
            int secondFittest = 0;
            Parameters fittestParams;
            Parameters secondFittestParams;
            int fittestIndividualWage;
            int secondFittestIndividualWage;
            List<Individual> parents = new ArrayList<>();
            for(int i = 0; i < populationSize * 0.1; i++)
                parents.add(this.individuals.get(randMachine.nextInt(this.individuals.size())));

            for(int i = 0; i < parents.size(); i++) {
                if(parents.get(i).getFitness() > parents.get(fittest).getFitness()) {
                    fittest = i;
                }
            }
            fittestIndividualWage = parents.get(fittest).getFitness();
            fittestParams = parents.get(fittest).getGenome();
            parents.remove(fittest);
            for(int i = 0; i < parents.size(); i++) {
                if(parents.get(i).getFitness() > parents.get(secondFittest).getFitness()) {
                    secondFittest = i;
                }
            }
            secondFittestIndividualWage = parents.get(secondFittest).getFitness();
            secondFittestParams = parents.get(secondFittest).getGenome();

            Individual child = new Individual();
            // I know this is ugly. It's just simple wage average for every two parameters of parents.
            // I'm just avoiding unnecessary assignment every new parameter for double variable.
            // Let's calculate: 4 parameters * 300 new individuals = 1200 assignments
            // In, like, 1000 generations its 1 200 000 assignments!!!!!
            child.setGenome(new Parameters((fittestParams.getAggregateHeightWage() * fittestIndividualWage + secondFittestParams.getAggregateHeightWage() * secondFittestIndividualWage) / (fittestIndividualWage + secondFittestIndividualWage),
                    (fittestParams.getFullLinesWage() * fittestIndividualWage + secondFittestParams.getFullLinesWage()) / (fittestIndividualWage + secondFittestIndividualWage),
                    (fittestParams.getHolesWage() * fittestIndividualWage + secondFittestParams.getHolesWage() * secondFittestIndividualWage) / (fittestIndividualWage + secondFittestIndividualWage),
                    (fittestParams.getBumpinessWage() * fittestIndividualWage + secondFittestParams.getBumpinessWage() * secondFittestIndividualWage) / (fittestIndividualWage + secondFittestIndividualWage)));
            offsprings.add(child);
        }

        for(int i = 0; i < populationSize * 0.3; i++) {
            int weakest = 0;
            for(int k = 0; k < this.individuals.size(); k++) {
                if(individuals.get(k).getFitness() < individuals.get(weakest).getFitness()) weakest = k;
            }
            individuals.remove(weakest);
        }

        // mutation process
        for(int i = 0; i < offsprings.size(); i++) {
            double stolenChromosomeChance = randMachine.nextDouble();
            if(stolenChromosomeChance <= 0.05)
                offsprings.get(i).mutate();
        }
        for(Individual ind : offsprings)
            this.individuals.add(ind);
    }
}
