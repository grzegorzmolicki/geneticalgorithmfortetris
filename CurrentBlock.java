import java.util.Random;

/**
 * Created by greg on 04.01.16.
 */
public class CurrentBlock {
    private int posX;
    private int posY;
    private int[][] brick;
    private int type;
    Random randMachine;

    private void generateNewRandomBlock() {
        this.type = randMachine.nextInt(7);
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                this.brick[i][j] = 0;
        this.posX = 0;
        this.posY = 0;
        switch(this.type) {
            case 0:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[2][0] = 1;
                this.brick[3][0] = 1;
                break;
            case 1:
                this.brick[0][0] = 1;
                this.brick[0][1] = 1;
                this.brick[0][2] = 1;
                this.brick[1][1] = 1;
                break;
            case 2:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                break;
            case 3:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[2][0] = 1;
                this.brick[2][1] = 1;
                break;
            case 4:
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                this.brick[2][1] = 1;
                this.brick[2][0] = 1;
                break;
            case 5:
                this.brick[0][1] = 1;
                this.brick[0][2] = 1;
                this.brick[1][0] = 1;
                this.brick[1][1] = 1;
                break;
            case 6:
                this.brick[0][0] = 1;
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                this.brick[1][2] = 1;
                break;
        }
    }

    private void generateNewRandomBlock(int blockType) {
        this.type = blockType;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                this.brick[i][j] = 0;
        this.posX = 0;
        this.posY = 0;
        switch(this.type) {
            case 0:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[2][0] = 1;
                this.brick[3][0] = 1;
                break;
            case 1:
                this.brick[0][0] = 1;
                this.brick[0][1] = 1;
                this.brick[0][2] = 1;
                this.brick[1][1] = 1;
                break;
            case 2:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                break;
            case 3:
                this.brick[0][0] = 1;
                this.brick[1][0] = 1;
                this.brick[2][0] = 1;
                this.brick[2][1] = 1;
                break;
            case 4:
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                this.brick[2][1] = 1;
                this.brick[2][0] = 1;
                break;
            case 5:
                this.brick[0][1] = 1;
                this.brick[0][2] = 1;
                this.brick[1][0] = 1;
                this.brick[1][1] = 1;
                break;
            case 6:
                this.brick[0][0] = 1;
                this.brick[0][1] = 1;
                this.brick[1][1] = 1;
                this.brick[1][2] = 1;
                break;
        }
    }

    private int getMaxRightIndex() {
        int maxRight = 0;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                if(this.brick[i][j] != 0 && j > maxRight) maxRight = j;

        return maxRight;
    }

    public CurrentBlock(int type) {
        this.brick = new int[4][4];
        this.randMachine = new Random();
        generateNewRandomBlock(type);
    }

    public void moveDown(int grid[][]) {
        boolean isLowerable = true;

        while(isLowerable) {
            for(int i = 0; i < 4; i++)
                for(int j = 0; j < 4; j++) {
                    if((this.brick[i][j] != 0) && (grid[i+1+this.posY][j+this.posX] != 0)) {
                        isLowerable = false;
                        break;
                    }
                }
            if(isLowerable) this.posY++;
        }
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                if(this.brick[i][j] != 0) grid[i+this.posY][j+this.posX] = this.brick[i][j];

        this.generateNewRandomBlock();
    }

    public void move(int grid[][], int dir) {
        boolean isMovable = true;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                if(this.brick[i][j] != 0 && (grid[i+this.posY][j+this.posX] != 0)) isMovable = false;
        if(isMovable) {
            this.posX += dir;
            if(this.posX < 0) this.posX = 0;
            if(this.posX+getMaxRightIndex() > 9) this.posX = 9-getMaxRightIndex();
        }
    }

    public void rotate(int grid[][]) {
        int[][] tempBrick = new int[4][4];
        int[][] temponary = new int[4][4];
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                temponary[i][j] = this.brick[j][i];

        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                tempBrick[i][j] = temponary[3-i][j];

        while((tempBrick[0][0] == 0) && (tempBrick[1][0] == 0) && (tempBrick[2][0] == 0) && (tempBrick[3][0] == 0)) {
            for(int i = 0; i < 4; i++)
                for(int j = 1; j < 4; j++)
                    tempBrick[i][j-1] = tempBrick[i][j];
            tempBrick[0][3] = tempBrick[1][3] = tempBrick[2][3] = tempBrick[3][3] = 0;
        }

        while((tempBrick[0][0] == 0) && (tempBrick[0][1] == 0) && (tempBrick[0][2] == 0) && (tempBrick[0][3] == 0)) {
            for(int i = 1; i < 4; i++)
                for(int j = 0; j < 4; j++)
                    tempBrick[i-1][j] = tempBrick[i][j];
            tempBrick[3][0] = tempBrick[3][1] = tempBrick[3][2] = tempBrick[3][3] = 0;
        }
        boolean isRotatable = true;
        if(this.posX > (10-getMaxRightIndex()-1)) isRotatable = false;
        for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
                if((tempBrick[i][j] != 0) && (grid[this.posY+i][this.posX+j] != 0)) isRotatable = false;

        if(isRotatable) {
            for(int i = 0; i < 4; i++)
                for(int j = 0; j < 4; j++)
                    this.brick[i][j] = tempBrick[i][j];
        }
    }

    public int getType() {
        return this.type;
    }
}
