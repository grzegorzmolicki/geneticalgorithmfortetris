See app in action here:
https://www.youtube.com/watch?v=Xr9D5dtfB3g

I have no interest in implementing smooth blocks movement, because my goal is genetic algorithm, rather than tetris game itself so if you want to add this, just let me know.

How to control app is written in "about *" tab.