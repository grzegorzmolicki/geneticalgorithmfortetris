import javax.swing.*;
import java.awt.*;

/**
 * Created by greg on 19.01.16.
 */
public class AboutSimulation extends JPanel {
    public static final int WIDTH = 700;
    private static final int BORDER = 100;

    private JTextArea about;

    private JPanel panel;

    public AboutSimulation() {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        setPreferredSize(new Dimension(WIDTH, screenSize.height-BORDER));
        setLayout(new BorderLayout());

        about = new JTextArea("In general simulation is all about evolving, breeding individuals with each other and waiting." +
                "It's small civilization which goal is to be the best tetris players in the world." +
                "\nPopulation size is abvious. 10 means that there is 10 individuals in society." +
                "\nGames for each limits how many games every individual plays in one generation." +
                "\nThreads is only for faster calculations. If you have powerfull PC you can set this to 10-20 and even more," +
                "but when you set it greater than population size you will just waste thread and/or computer memory." +
                "\nBest so far points best individual and it's result in n-games, so if you want to know how many lines this one completed you should divide result by quantity of games for each individual." +
                "\nStop button when activated stops generation counting process when the last of individuals ends it's games.");
        about.setWrapStyleWord(true);
        about.setLineWrap(true);
        about.setEditable(false);

        panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        panel.setPreferredSize(new Dimension(WIDTH, screenSize.height-BORDER));
        panel.add(about);

        add(panel, BorderLayout.NORTH);
    }
}
