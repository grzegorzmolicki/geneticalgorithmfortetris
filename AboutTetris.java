import javax.swing.*;
import java.awt.*;

/**
 * Created by greg on 19.01.16.
 */
public class AboutTetris extends JPanel {
    public static final int WIDTH = 700;
    private static final int BORDER = 100;

    private JTextArea about;

    private JPanel panel;

    public AboutTetris() {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        setPreferredSize(new Dimension(WIDTH, screenSize.height-BORDER));
        setLayout(new BorderLayout());

        about = new JTextArea("Oh come on. Everyone knows what tetris is..." +
                "\nThe only thing you need to know is that 'Start Best' button starts game with wages evolved by many, many generations" +
                "counted on my friends PC (Thanks Patryk), becase my C2D is, well, very weak." +
                "\nThe 'Start' button starts game with wages written in these fields." +
                "\nAggregateHeight - wage is used in multiplication accumulated each column wage on a grid" +
                "\nFullLines - wage is used in multiplication each cleared line on a grid" +
                "\nHoles - well, this is wage of every single hole in grid. It's quite obvious - I think." +
                "\nBumpiness - this one is responsible for roughness of blocks on grid" +
                "\nTry to type your own ones and watch what happens. Fields accepts floating-points numbers with exponential notation.");

        about.setWrapStyleWord(true);
        about.setLineWrap(true);
        about.setEditable(false);
        panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        panel.setPreferredSize(new Dimension(WIDTH, screenSize.height-BORDER));
        panel.add(about);

        add(panel, BorderLayout.NORTH);
    }
}
