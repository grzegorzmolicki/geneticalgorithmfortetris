import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by greg on 19.01.16.
 */
public class Simulation extends JPanel {
    public static final int WIDTH = 800;
    private static final int BORDER = 100;

    static final int MAXPOPULATION = 10000;
    static final int MINPOPULATION = 40;
    static final int INITPOPULATION = 100;

    private SpinnerModel populationSpinnerProt;
    private JSpinner populationSpinner;

    private SpinnerModel gameSpinnerProt;
    private JSpinner gameSpinner;

    private JSpinner threadSpinner;

    private JLabel gamesLabel;
    private JLabel populationLabel;
    private JLabel workingStatus;
    private JLabel generationsCounterLabel;
    private JLabel threadsCounterLabel;

    private JPanel inputPanel;

    private JButton startButton;
    private JButton stopButton;

    private JTextArea fittestStats;

    private JProgressBar generationProgressBar;

    private Population pupils;
    private Thread populationThread;

    public Simulation() {
        Toolkit kit = Toolkit.getDefaultToolkit();
        Dimension screenSize = kit.getScreenSize();

        setPreferredSize(new Dimension(WIDTH, screenSize.height-BORDER));
        setLayout(new BorderLayout());

        populationLabel = new JLabel("Population size:");
        populationSpinnerProt = new SpinnerNumberModel(INITPOPULATION, MINPOPULATION, MAXPOPULATION, 10);
        populationSpinner = new JSpinner(populationSpinnerProt);

        gamesLabel = new JLabel("Games for each:");
        gameSpinnerProt = new SpinnerNumberModel(50, 10, 1000, 1);
        gameSpinner = new JSpinner(gameSpinnerProt);

        threadsCounterLabel = new JLabel("Threads:");
        threadSpinner = new JSpinner(new SpinnerNumberModel(4, 1, 1000, 1));

        workingStatus = new JLabel("Not working :(");

        startButton = new JButton("Start");
        stopButton = new JButton("Stop");

        generationsCounterLabel = new JLabel("Generation: 0");
        fittestStats = new JTextArea();
        fittestStats.setEditable(false);

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(populationThread == null) {
                    pupils = new Population((Integer)populationSpinner.getValue(), (Integer)gameSpinner.getValue(), (Integer)threadSpinner.getValue(), true, generationsCounterLabel, generationProgressBar, fittestStats);
                    populationThread = new Thread(pupils);
                    workingStatus.setText("Working :)");
                    if(populationThread.getState().equals(Thread.State.NEW))
                        populationThread.start();
                }
            }
        });

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                workingStatus.setText("Not working :(");
                populationThread.interrupt();
                populationThread = null;
            }
        });

        generationProgressBar = new JProgressBar(0, 100);
        generationProgressBar.setValue(0);
        generationProgressBar.setString("Generation progress");
        generationProgressBar.setStringPainted(true);

        inputPanel = new JPanel();
        inputPanel.setLayout(new GridLayout(6, 2));
        inputPanel.setPreferredSize(new Dimension(WIDTH/2, 120));

        inputPanel.add(populationLabel);
        inputPanel.add(populationSpinner);
        inputPanel.add(gamesLabel);
        inputPanel.add(gameSpinner);
        inputPanel.add(threadsCounterLabel);
        inputPanel.add(threadSpinner);

        inputPanel.add(startButton);
        inputPanel.add(stopButton);
        inputPanel.add(generationProgressBar);
        inputPanel.add(workingStatus);

        inputPanel.add(generationsCounterLabel);

        add(inputPanel, BorderLayout.NORTH);
        add(fittestStats, BorderLayout.CENTER);
    }
}
