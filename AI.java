import java.util.Arrays;
import java.lang.Math;

/**
 * Created by greg on 04.01.16.
 */
public class AI {
    private double aggregateHeightWage;
    private double fullLinesWage;
    private double holesWage;
    private double bumpinessWage;


    public AI(Parameters params) {
        this.aggregateHeightWage = params.getAggregateHeightWage();
        this.fullLinesWage = params.getFullLinesWage();
        this.holesWage = params.getHolesWage();
        this.bumpinessWage = params.getBumpinessWage();
    }

    private int calculateAggregateHeight(int grid[][]) {
        int aggregateHeight = 0;
        for(int i = 0; i < 10; i++)
            for(int j = 0; j < 20; j++)
                if(grid[j][i] != 0) {
                    aggregateHeight += 20-j;
                    break;
                }
        return aggregateHeight;
    }

    public int checkLine(int grid[][]) {
        int lines = 0;
        for(int i = 19; i >= 0; i--) {
            boolean wholeLine = true;
            for(int j = 0; j < 10; j++)
                if(grid[i][j] == 0) wholeLine = false;

            if(wholeLine) {
                lines++;
                for(int j = 0; j < 10; j++)
                    grid[i][j] = 0;
                for(int k = i; k > 0; k--)
                    for(int j = 0; j < 10; j++)
                        grid[k][j] = grid[k-1][j];
                i++;
            }
        }
        return lines;
    }

    private int countHoles(int grid[][]) {
        int holesCounter = 0;
        for(int i = 0; i < 10; i++)
            for(int j = 0; j < 19; j++) {
                if(grid[j][i] != 0 && grid[j+1][i] == 0) {
                    while(grid[j+1][i] == 0) {
                        holesCounter++;
                        j++;
                    }
                }
            }
        return holesCounter;
    }

    private int countBumpiness(int grid[][]) {
        int[] heights = new int[10];
        Arrays.fill(heights, 0);

        for(int i = 0; i < 10; i++)
            for(int j = 0; j < 20; j++) {
                if(grid[j][i] != 0) {
                    heights[i] = 20-j;
                    break;
                }
            }
        int bumpiness = 0;
        for(int i = 0; i < heights.length-1; i++)
            bumpiness += Math.abs(heights[i] - heights[i+1]);

        return bumpiness;
    }

    private double calculateResult(int grid[][]) {
        return (this.aggregateHeightWage*calculateAggregateHeight(grid)+this.bumpinessWage*countBumpiness(grid)+this.fullLinesWage*checkLine(grid)+this.holesWage*countHoles(grid));
    }

    public void makeAIMove(int grid[][], CurrentBlock block) {
        int bestRotations = 0;
        int bestRightMove = 0;
        double bestResult;

        int[][] tempGrid = new int[21][10];
        for(int i = 0; i < 21; i++)
            for(int j = 0; j < 10; j++)
                tempGrid[i][j] = grid[i][j];
        CurrentBlock tmpBlock = new CurrentBlock(block.getType());
        tmpBlock.moveDown(tempGrid);
        bestResult = calculateResult(tempGrid);

        for(int rotations = 0; rotations < 4; rotations++) {
            for(int column = 0; column < 12; column++) {
                for(int rows = 0; rows < 21; rows++)
                    for(int cols = 0; cols < 10; cols++)
                        tempGrid[rows][cols] = grid[rows][cols];

                CurrentBlock tempBlock = new CurrentBlock(block.getType());
                for(int rot = 0; rot < rotations; rot++)
                    tempBlock.rotate(tempGrid);

                for(int moves = 0; moves < column; moves++)
                    tempBlock.move(tempGrid, 1);
                tempBlock.moveDown(tempGrid);

                double result = calculateResult(tempGrid);
                if(bestResult < result) {
                    bestResult = result;
                    bestRightMove = column;
                    bestRotations = rotations;
                }
            }
        }
        for(int rotations = 0; rotations < bestRotations; rotations++)
            block.rotate(grid);

        for(int rightMoves = 0; rightMoves < bestRightMove; rightMoves++)
            block.move(grid, 1);
        block.moveDown(grid);
    }
}
