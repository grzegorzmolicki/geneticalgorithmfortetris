/**
 * Created by greg on 04.01.16.
 */
public class Parameters {
    private double aggregateHeightWage;
    private double fullLinesWage;
    private double holesWage;
    private double bumpinessWage;

    public Parameters(double aggregateHeightWage, double fullLinesWage, double holesWage, double bumpinessWage) {
        this.aggregateHeightWage = aggregateHeightWage;
        this.fullLinesWage = fullLinesWage;
        this.holesWage = holesWage;
        this.bumpinessWage = bumpinessWage;
    }

    public void setAggregateHeightWage(double aggregateHeightWage) {
        this.aggregateHeightWage = aggregateHeightWage;
    }

    public void setFullLinesWage(double fullLinesWage) {
        this.fullLinesWage = fullLinesWage;
    }

    public void setHolesWage(double holesWage) {
        this.holesWage = holesWage;
    }

    public void setBumpinessWage(double bumpinessWage) {
        this.bumpinessWage = bumpinessWage;
    }

    public double getAggregateHeightWage() {
        return this.aggregateHeightWage;
    }

    public double getFullLinesWage() {
        return this.fullLinesWage;
    }

    public double getHolesWage() {
        return this.holesWage;
    }

    public double getBumpinessWage() {
        return this.bumpinessWage;
    }
}
